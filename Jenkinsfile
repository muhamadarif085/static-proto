pipeline {
  environment {
    PROJECT = 'practice-357012'
    APP_NAME = 'simple-porto'
    DOCKER_REPO = 'arifpradana22'
    CLUSTER = 'jenkins-demo'
    CLUSTER_ZONE = 'us-east1-d'
    IMAGE_TAG = "docker.io/${DOCKER_REPO}/${APP_NAME}:${env.BRANCH_NAME}.${env.BUILD_NUMBER}"
    JENKINS_CRED = "${PROJECT}"
    DOCKERHUB_CRED = credentials('dockerhub')
  }

  agent {
    kubernetes {
      inheritFrom 'simple-porto'
      defaultContainer 'jnlp'
      yaml '''
apiVersion: v1
kind: Pod
metadata:
labels:
  component: ci
spec:
  # Use service account that can deploy to all namespaces
  serviceAccountName: cd-jenkins
  containers:
  - name: gcloud
    image: gcr.io/cloud-builders/gcloud
    command:
    - cat
    tty: true
  - name: kubectl
    image: gcr.io/cloud-builders/kubectl
    command:
    - cat
    tty: true
  - name: docker
    image: docker:latest
    command:
    - cat
    tty: true
    volumeMounts:
    - mountPath: /var/run/docker.sock
      name: docker-sock
  volumes:
  - name: docker-sock
    hostPath:
      path: /var/run/docker.sock
'''
    }
  }

  stages {
    stage('Login, Build, Push Docker') {
        steps {
            container('docker'){
                sh '''
            docker login -u ${DOCKERHUB_CRED_USR} -p ${DOCKERHUB_CRED_PSW}
            docker build -t ${IMAGE_TAG} .
            docker push ${IMAGE_TAG}
            '''
            }
        }
    }
    stage('Deploy static web to GKE') {
      steps {
        container('kubectl') {
          sh("sed -i.bak 's#arifpradana22/simple-porto:1.0.0#${IMAGE_TAG}#' ./deployment/*.yaml")
          step([$class: 'KubernetesEngineBuilder',
                            // namespace:'canary',
                            projectId: env.PROJECT,
                            clusterName: env.CLUSTER,
                            zone: env.CLUSTER_ZONE,
                            manifestPattern: 'deployment',
                            credentialsId: env.JENKINS_CRED,
                            verifyDeployments: true])
        }
      }
    }
  }
  post {
        always {
            container('docker') {
                sh '''
            docker logout
            docker system prune -f --all
            '''
                }
        }
    }
}
